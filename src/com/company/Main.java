package com.company;

public class Main {
    public volatile static int balance = 10;
    public static boolean flagThread = false;
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    if ((!(balance == 10))){
                        balance = balance + 10;
                        if (flagThread){
                            System.out.println(balance);
                            flagThread = false;
                        }

                    }
                }
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){

                    if (balance == 10){
                        balance = balance - 10;
                        if (!flagThread){
                            System.out.println(balance);
                            flagThread = true;
                        }

                    }
                }
            }
        });
        thread1.start();
        thread2.start();
    }

}
